import express from 'express';
import mongoose from "mongoose";
import bodyParser from "body-parser";
import Promise from "bluebird";
import cors from 'cors';
const http = require('http')
import ip from 'ip'

import SocketService from './services/socket/'

const request = require('request-promise');

import config from './config'

import routes from './routes'

const app = express();
app.use(bodyParser.json());
mongoose.Promise = Promise;



mongoose.connect(config.mongodb_url, { useUnifiedTopology: true })
    .then(() => {
        console.log('connected to db....')
    })
    .catch(() => {
        console.log('something wrong...')
    });

app.use(cors());

ip.mask('192.168.1.134', '255.255.255.0')

routes.forEach(r => {
    app.use(r.path, r.route);
});

const server = http.createServer(app)

app.use('/', (req, res)=> {
  console.log('connect from arduino')
  res.status(200).json({ status: 'ok' })
})


server.listen(8080, () => {
  console.log('Server has been running on port 8080')
  console.log(`server ip address is ${ip.address()}`)
  SocketService.init(server)
});



