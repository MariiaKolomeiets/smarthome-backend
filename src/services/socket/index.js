import socketIO from 'socket.io';

import ClientManager from './clientManager'
import ErrorService from '../errors';

const clientManager = new ClientManager()

function SocketBoard() {
  let io = null;

  const init = (server) => {
    io = socketIO(server)
    io.on('connection',connectionHandler)
  }

  const connectionHandler = (client) => {
    console.log('New client connected')

    // clientManager.addClient(client);

    client.on('registerDevice', device => clientManager.registerDevice(client, device))

    // ===================================

    client.on('registerClient', user => clientManager.registerClient(client, user))
    client.on('removeClient', () => clientManager.removeClient(client))

    client.on('brightness', (data) => changeBrightness(data))
    client.on('lightMode', (data) => changeLightMode(data))
    client.on('lightSpeed', (data) => changeLightSpeed(data))
  }

  const changeLightSpeed = data => {
    const { deviceId, value } = data;
    console.log(data)
    // const client = clientManager.getDevice(deviceId);
    // client.emit('speed', value)
  }

  const changeLightMode = data => {
    const { deviceId, value } = data;
    console.log(data)
    // const client = clientManager.getDevice(deviceId);
    // client.emit('lightMode', value)
  }

  const changeBrightness = data => {
    const { deviceId, value } = data;
    console.log(data)
    // const client = clientManager.getDevice(deviceId);
    // client.emit('brightness', value)
  }

  return {
    init
  }
}

export default new SocketBoard()
