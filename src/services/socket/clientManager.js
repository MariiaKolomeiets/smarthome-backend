module.exports = function () {
  // mapping of all connected clients
  const clients = new Map()
  const devices = new Map()

  function addClient(client) {
    clients.set(client.id, { client })
  }

  function registerClient(client, user) {
    clients.set(client.id, { client, user })
  }

  function registerDevice(client, device) {
    devices.set(client.id, { client, device })
  }

  function getDevice(deviceName) {
    // devices.set(client.id, { client, device })
    Array.from(devices.values())
      .filter(d => d.device.name === deviceName)
      .map(d => d.client)
  }

  function removeClient(client) {
    clients.delete(client.id)
  }

  function getAvailableDevices() {
    console.log(clients)
    const usersTaken = new Set(
      Array.from(devices.values())
        .filter(c => c.device)
    )
    console.log(usersTaken)
  }

  return {
    addClient,
    registerClient,
    removeClient,
    getAvailableDevices,
    registerDevice,
    getDevice,
  }
}
