class Errors {
  constructor() {
    this.errors  = [
      {
        errorCode: '2',
        errorMessage: 'Ventilation error',
      },
      {
        errorCode: '6',
        errorMessage: 'Lightening error',
      },
      {
        errorCode: '9',
        errorMessage: "Plug don't work",
      },
      {
        errorCode: '2',
        errorMessage: 'Ventilation error',
      },

    ];
  }

  toogleError(errorCode) {
    const errorCodes = this.errors.map(e => e.errorCode)
    if (errorCodes.includes(errorCode)) {
      this.errors = this.errors.filter(e => e.errorCode !== errorCode)
    } else this.errors.push({
      errorCode,
      errorMessage: "DTh don't work",
    });

  }

  getErrors() {
    return this.errors;
  }
}

export default new Errors();
