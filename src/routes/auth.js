import express from "express";
import User from "../models/User";

const router = express.Router();

router.post("/", async (req, res) => {
  console.log(req.body);
  const { email,password } = req.body;
  const user = await User.findOne({ email });

  if (user && user.isValidPassword(password)) {
      res.json({user: user.toAuthJSON(user.id)});
  } else {
    res.status(400).json({ errors: { global: "Invalid credentials" } });
  }
});

export default router;
