import auth from './auth'
import user from './users'
const routes = [
    {
        route: auth,
        path: '/api/auth',
        middlewares: []
    },
    {
        route: user,
        path: '/api/user',
    },
]

export default routes;
