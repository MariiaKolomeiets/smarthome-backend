import express from "express";
import User from "../models/User";
import parseErrors from "../utils/parseErrors";

const router = express.Router();

router.post("/", async (req, res) => {
    try {
        const {email, password,role} = req.body;
        const user = new User({email,role});
        user.setPassword(password);
        const userRecord = await user.save();
        res.json({user: userRecord.toAuthJSON(user.id)});
    } catch (err) {
        res.status(400).json({errors: parseErrors(err.errors)})
    }
});



export default router;
