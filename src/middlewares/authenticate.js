import jwt from "jsonwebtoken";
import User from "../models/User";
import config from '../config'

export default (req, res, next) => {
  console.log(req)
  const header = req.get('Authorization');
  let token;

  if (header) token = header.split(" ")[1];

  if (token) {
    jwt.verify(token, config.secret_key, (err, decoded) => {
      if (err) {
        res.status(401).json({ errors: { global: "Invalid token" } });
      } else {
        User.findOne({ email: decoded.email }).then(user => {
          req.currentUser = user;
          next();
        });
      }
    });
  } else {
    res.status(401).json({ errors: { global: "No token" } });
  }
};
