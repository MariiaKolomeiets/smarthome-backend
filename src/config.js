export default {
  host: 8080,
  secret_key: 'secret_key',
  mongodb_url: 'mongodb://localhost/smartHome'
}
