import mongoose, { Schema } from "mongoose";


const schema = new Schema(
    {
        systemType:{
            type: String
        },
        state: {
            type: Boolean,
            default: false
        },
        params:{
            type: Array,
            default: []
        },
        carriage:{
            type: Schema.Types.ObjectId,
            ref:'Carriage'
        },
        systemErrors:[{
            code:Number,
            message: String
        }]
    }
);


schema.methods.setParams = function addAppliance(params) {
    this.params.push(params);
};
export default mongoose.model("System", schema);
