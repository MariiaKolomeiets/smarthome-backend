import mongoose, {Schema} from "mongoose";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import uniqueValidator from "mongoose-unique-validator";

import config from '../config'

const schema = new Schema(
    {
        email: {
            type: String,
            required: true,
            lowercase: true,
            index: true,
            unique: true
        },
        password: {type: String, required: true},
        role: {
            type: String,
            required: true,
            lowercase: true
        }
    },
    {timestamps: true}
);

schema.methods.isValidPassword = function isValidPassword(password) {
    return bcrypt.compareSync(password, this.password);
};

schema.methods.setPassword = function setPassword(password) {
    this.password = bcrypt.hashSync(password, 10);
};

schema.methods.generateJWT = function generateJWT() {
    return jwt.sign(
        {
            email: this.email,
            confirmed: this.confirmed
        },
        config.secret_key
    );
};

schema.methods.generateResetPasswordToken = function generateResetPasswordToken() {
    return jwt.sign(
        {
            _id: this._id
        },
        config.secret_key,
        {expiresIn: "1h"}
    );
};

schema.methods.toAuthJSON = function toAuthJSON( userId) {
    return {
        email: this.email,
        accessToken: this.generateJWT(),
        userId
    };
};

schema.plugin(uniqueValidator, {message: "This email is already taken"});

export default mongoose.model("User", schema);
